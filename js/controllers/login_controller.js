angular.module('DemoLibrary').controller('LoginController', ['$scope', 'AlertMe', 'FirebaseService', '$location', function ($scope, AlertMe, FirebaseService, $location) { // eslint-disable-line no-undef
  // if alredy logged in redirect to dashboard
  var user = FirebaseService.getSignedInUser()
  if (user) $location.path('/dashboard/books')

  // master user object
  $scope.masterUser = {}

  // on login form submit
  $scope.login = function () {
    if ($scope.user && $scope.user.username && $scope.user.password) {
      $scope.masterUser = angular.copy($scope.user) // eslint-disable-line no-undef
      FirebaseService.signIn($scope.masterUser.username, $scope.masterUser.password, $scope.onLoginCompleted)
    } else {
      var alertConfig = {
        content: 'Invalid username/password combination!',
        className: 'warning'
      }
      AlertMe.create(alertConfig)
    }
  }

  firebase.auth().onAuthStateChanged(function (user) { // eslint-disable-line no-undef
    $scope.onLoginCompleted(user)
  })

  // gets called when Firebase returns an user object
  $scope.onLoginCompleted = function (user) {
    if (user) {
      // route the user to the dashboard view
      $location.path('/dashboard/books')
      $scope.$apply()
    }
  }
}])
