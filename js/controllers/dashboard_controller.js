angular.module('DemoLibrary').controller('DashboardController', ['$scope', 'AlertMe', 'FirebaseService', '$location', 'ngDialog', function ($scope, AlertMe, FirebaseService, $location, ngDialog) { // eslint-disable-line no-undef

  // if user has not logged in redirect them to the login page
  angular.element(function () { // eslint-disable-line
    var user = FirebaseService.getSignedInUser()
    if (!user) $location.path('/')
    $scope.$evalAsync()
  })

  $scope.signOut = function () {
    FirebaseService.signOut($scope.onSignOut)
  }

  $scope.onSignOut = function (error) {
    if (error) {
      var alertConfig = {
        content: 'Something wen\'t wrong while trying to sign you out',
        className: 'warning'
      }
      AlertMe.create(alertConfig)
    } else {
      $location.path('/')
      $scope.$apply()
    }
  }

  $scope.showAddBookDialog = function () {
    ngDialog.open({
      template: 'partials/add_book.html',
      className: 'ngdialog-theme-default',
      controller: ['$scope', 'FirebaseService', 'AlertMe', function ($scope, FirebaseService, AlertMe) {
        $scope.newBook = function () {
          if ($scope.book) {
            $scope.book.timestamp = FirebaseService.getServerTime()
            var unixTime = $scope.printed_date / 1 // converting to unix
            $scope.book.printed_date = unixTime
            console.log(unixTime)
            FirebaseService.addBook($scope.book, function (e) {
              if (e) {
                var alertConfig = {
                  content: 'Oops! Something went wrong!',
                  className: 'warning'
                }
                AlertMe.create(alertConfig)
              } else {
                $scope.closeThisDialog()
                var alertConfig2 = {
                  content: 'Changes saved!',
                  className: 'success'
                }
                AlertMe.create(alertConfig2)
              }
            })
          }
        }
      }]
    })
  }

  // lilsten for db reference data changes and update data locally
  var ref = FirebaseService.retrieveDbObj('books')
  ref.on('value', function (snapshot) {
    $scope.books = snapshot.val()
    $scope.$evalAsync()
  })

  // method to remove a book
  $scope.removeBook = function (bookId) {
    if (window.confirm('are you sure you want to delete this item?')) {
      var bookRef = FirebaseService.retrieveDbObj('books/' + bookId)
      bookRef.remove(function (e) {
        if (e) {
          var alertConfig = {
            content: 'Oops! Something went wrong!',
            className: 'warning'
          }
          AlertMe.create(alertConfig)
        } else {
          var alertConfig2 = {
            content: 'Changes saved!',
            className: 'success'
          }
          AlertMe.create(alertConfig2)
        }
      })
    } else {

    }
  }
}])
