angular.module('DemoLibrary').factory('FirebaseService', function (AlertMe) { // eslint-disable-line no-undef
  return {
    signIn: function (email, password, callback) {
      firebase.auth().signInWithEmailAndPassword(email, password).catch(function (error) { // eslint-disable-line no-undef
        AlertMe.create({
          content: 'Invalid username/password combination!',
          className: 'warning'
        })
        console.log('Auth Error', error)
      })
    },
    getSignedInUser: function () {
      var user = firebase.auth().currentUser // eslint-disable-line no-undef
      if (user) return user
      else return undefined
    },
    signOut: function (callback) {
      firebase.auth().signOut().then(function () { // eslint-disable-line no-undef
        callback(undefined)
      }).catch(function (error) {
        callback(error)
      })
    },
    getServerTime: function () {
      return firebase.database.ServerValue.TIMESTAMP // eslint-disable-line no-undef
    },
    addBook: function (book, onSuccess) {
      var ref = firebase.database().ref('books') // eslint-disable-line no-undef
      var newBook = ref.push()
      newBook.set(book, function (e) {
        onSuccess(e)
      })
    },
    retrieveDbObj: function (id) {
      return firebase.database().ref(id) // eslint-disable-line no-undef
    }
  }
})
