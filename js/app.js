var app = angular.module('DemoLibrary', ['ngRoute', 'alert-me', 'ngDialog']) // eslint-disable-line no-undef
app.config(['$routeProvider', function ($routeProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'partials/login.html',
      controller: 'LoginController'
    })
    .when('/dashboard/:id', {
      templateUrl: 'partials/dashboard.html',
      controller: 'DashboardController'
    })
}])

// Initialize Firebase
var config = {
  apiKey: 'AIzaSyC01I6uT4iuEBIdYTCyfQQA58gtPwKdLdw',
  authDomain: 'fir-library-74766.firebaseapp.com',
  databaseURL: 'https://fir-library-74766.firebaseio.com',
  projectId: 'fir-library-74766',
  storageBucket: 'fir-library-74766.appspot.com',
  messagingSenderId: '98342463527'
}
firebase.initializeApp(config) // eslint-disable-line no-undef
