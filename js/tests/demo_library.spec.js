describe('Testing Demo Library:', function () {
  beforeEach(module('DemoLibrary'))

  describe('Testing login page:', function () {
    var scope, loginController, alertMe, firebase

    beforeEach(function () {
      inject(function ($controller, $rootScope, AlertMe, FirebaseService) {
        scope = $rootScope.$new()
        loginController = $controller('LoginController', {$scope: scope})
        alertMe = AlertMe
        firebase = FirebaseService
      })
    })

    it('Initially username and password should be empty', function () {
      expect(scope.masterUser).toBeDefined()
      expect(scope.user).toBeUndefined()
    })

    it('Login method should be defined', function () {
      expect(scope.login).toBeDefined()
    })

    it('should display alert when user object is undefined', function () {
      spyOn(alertMe, 'create')

      scope.login()

      expect(alertMe.create).toHaveBeenCalledWith({
        content: 'Invalid username/password combination!',
        className: 'warning'
      })
    })

    it('should update the master user when user object is present', function () {
      scope.user = {
        username: 'admin@demolibrary.com',
        password: 'pass123'
      }

      spyOn(firebase, 'signIn').and.callFake(function (username, password, callback) {
        callback(undefined)
      })

      scope.login()

      expect(scope.masterUser).toBeDefined()
      expect(scope.masterUser.username).toBe('admin@demolibrary.com')
      expect(scope.masterUser.password).toBe('pass123')
    })

    it('should invoke firebase auth and show error upon invalid credentials', function () {
      scope.user = {
        username: 'admin@demolibrary.com',
        password: 'pass123'
      }

      spyOn(firebase, 'signIn').and.callFake(function (username, password, callback) {
        callback(undefined)
      })

      spyOn(scope, 'onLoginCompleted').and.callThrough()
      spyOn(alertMe, 'create')

      scope.login()

      expect(alertMe.create).toHaveBeenCalled()
      
      expect(scope.onLoginCompleted).toHaveBeenCalledWith(undefined)
    })
  })

  describe('Testing firebase service:', function () {
    var firebase

    beforeEach(function () {
      inject(function (FirebaseService) {
        firebase = FirebaseService
      })
    })

    it('FirebaseService should have all valid methods defined', function () {
      expect(firebase).toBeDefined()
      expect(firebase.signIn).toBeDefined()
      expect(firebase.getSignedInUser).toBeDefined()
    })
  })
})
